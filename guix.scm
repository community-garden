;;; Commentary:
;;
;; Development environment for GNU Guix.
;;
;; To setup a development environment and build, run:
;;
;;    guix shell
;;    ./bootstrap
;;    ./configure
;;    make -j${nproc}
;;
;; To build the development snapshot, run:
;;
;;    guix build -f guix.scm
;;
;; To install the development snapshot, run:
;;
;;    guix install -f guix.scm
;;
;;; Code:
(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix build-system gnu)
             (guix download)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (gnu packages)
             (gnu packages audio)
             (gnu packages autotools)
             (gnu packages fontutils)
             (gnu packages gl)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages image)
             (gnu packages maths)
             (gnu packages mp3)
             (gnu packages pkg-config)
             (gnu packages readline)
             (gnu packages sdl)
             (gnu packages texinfo)
             (gnu packages xiph))

(define target-guile guile-3.0-latest)

(define guile3.0-opengl
  (package
    (inherit guile-opengl)
    (inputs
     (modify-inputs (package-inputs guile-opengl)
       (replace "guile" target-guile)))
    (native-inputs
     (modify-inputs (package-native-inputs guile-opengl)
       (append autoconf automake)))
    (arguments
     (substitute-keyword-arguments (package-arguments guile-opengl)
       ((#:phases phases)
        `(modify-phases ,phases
           (delete 'patch-makefile)
           (add-before 'bootstrap 'patch-configure.ac
             (lambda _
               ;; The Guile version check doesn't work for the 3.0
               ;; pre-release, so just remove it.
               (substitute* "configure.ac"
                 (("GUILE_PKG\\(\\[2.2 2.0\\]\\)") ""))
               (substitute* "Makefile.am"
                 (("\\$\\(GUILE_EFFECTIVE_VERSION\\)") "3.0")
                 (("ccache") "site-ccache"))
               #t))
           (replace 'bootstrap
             (lambda _
               (invoke "autoreconf" "-vfi")))))))))

(define guile-sdl2
  (let ((commit "e9a7f5e748719ce5b6ccd08ff91861b578034ea6"))
    (package
      (name "guile-sdl2")
      (version (string-append "0.7.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/guile-sdl2.git")
                      (commit commit)))
                (sha256
                 (base32
                  "0ay7mcar8zs0j5rihwlzi0l46vgg9i93piip4v8a3dzwjx3myr7v"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'bootstrap
                      (lambda _
                        (invoke "sh" "bootstrap"))))))
      (native-inputs (list autoconf automake pkg-config texinfo))
      (inputs (list target-guile sdl2))
      (synopsis "Guile bindings for SDL2")
      (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
      (home-page "https://git.dthompson.us/guile-sdl2.git")
      (license license:lgpl3+))))

(define chickadee
  (let ((commit "4047c0d0a92eae8a7394b4caa479d0cb6dd9017c"))
    (package
     (name "chickadee")
     (version (string-append "0.8.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/chickadee.git")
                    (commit commit)))
              (sha256
               (base32
                "14aa9bkwrsfl8i81zd0fzn0xryyhn8ac16hwdzx40p5az86nq7k6"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
                       (add-after 'unpack 'bootstrap
                                  (lambda _
                                    (invoke "sh" "bootstrap"))))))
     (native-inputs (list autoconf automake pkg-config texinfo))
     (inputs (list freetype
                   libjpeg-turbo
                   libpng
                   libvorbis
                   mpg123
                   openal
                   readline
                   target-guile))
     (propagated-inputs (list guile3.0-opengl guile-sdl2))
     (synopsis "Game development toolkit for Guile Scheme")
     (description "Chickadee is a game development toolkit for Guile
Scheme.  It contains all of the basic components needed to develop
2D/3D video games.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license license:gpl3+))))

(define catbird
  (let ((commit "0cbe11133c71cd853a3653a848edcf62a9d8b6b1"))
    (package
     (name "catbird")
     (version (string-append "0.1.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/catbird.git")
                    (commit commit)))
              (sha256
               (base32
                "09nf9yjyfpznvrsiwd7zr94c9hl8ncwzr56qxg0qbcq8yd6kmpj4"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
                       (add-after 'unpack 'bootstrap
                                  (lambda _
                                    (invoke "sh" "bootstrap"))))))
     (native-inputs (list autoconf automake pkg-config texinfo))
     (inputs (list target-guile))
     (propagated-inputs (list chickadee guile-sdl2))
     (synopsis "Game engine for Scheme programmers")
     (description "Catbird is a game engine written in Guile Scheme.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license license:gpl3+))))

(define guile-goblins
  (package
   (inherit guile-goblins)
   (name "guile-goblins")
   (version "0.9")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/spritely/guile-goblins.git")
                  (commit "9cbcd0614107e013aafea58fce0608f3b8baf635")))
            (file-name (string-append name "-" version))
            (sha256
             (base32
              "1k41gqd499q4vfpyzvk36syznn8k5qi7w6l8nq33z91g2bmqmz2v"))))))

(define %source-dir (dirname (current-filename)))

(package
 (name "community-garden")
 (version "0.1.0")
 (source (local-file %source-dir
                     #:recursive? #t
                     #:select? (git-predicate %source-dir)))
 (build-system gnu-build-system)
 (arguments
  '(#:make-flags '("GUILE_AUTO_COMPILE=0")
    #:phases
    (modify-phases %standard-phases
                   (add-after 'unpack 'bootstrap
                              (lambda _
                                (invoke "sh" "bootstrap"))))))
 (native-inputs (list autoconf automake pkg-config texinfo))
 (inputs (list target-guile))
 (propagated-inputs (list catbird guile-goblins))
 (synopsis "Spritely Goblins demo program")
 (description "Spritely Goblins demo program")
 (home-page "https://spritely.institute")
 (license license:asl2.0))
