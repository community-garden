(define-module (community-garden view)
  #:use-module (catbird)
  #:use-module (catbird asset)
  #:use-module (catbird camera)
  #:use-module (catbird input-map)
  #:use-module (catbird kernel)
  #:use-module (catbird mode)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (catbird region)
  #:use-module ((catbird scene)
                #:select (<scene> current-scene replace-major-mode))
  #:use-module (chickadee config)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics path)
  #:use-module (chickadee graphics text)
  #:use-module (chickadee graphics texture)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (community-garden garden-bed)
  #:use-module (community-garden plant)
  #:use-module (goblins)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (srfi srfi-43)
  #:export (view-garden))

(define %window-width 1024)
(define %window-height 768)
(define %tile-width 64.0)
(define %tile-height 64.0)
(define font-file (scope-datadir "fonts/Inconsolata-Regular.otf"))
(define-asset (title-font (f font-file))
  (load-font f 24))
(define-asset (plant-tile-font (f font-file))
  (load-font f 32))
(define-asset (sunflower-texture (f "assets/images/sunflower.png"))
  (load-image f))
(define-asset (cabbage-texture (f "assets/images/cabbage.png"))
  (load-image f))

(define-class <button> (<node-2d>)
  (on-click #:accessor on-click
            #:init-keyword #:on-click
            #:init-value (const #t)))

(define-class <garden-tile> (<node-2d>)
  (tile-x #:getter tile-x #:init-keyword #:tile-x)
  (tile-y #:getter tile-y #:init-keyword #:tile-y))

(define-class <garden-view> (<node-2d>)
  (vat #:getter vat #:init-keyword #:vat)
  (user #:getter user #:init-keyword #:user)
  (name-box #:getter name-box #:init-keyword #:name-box)
  (garden-bed-box #:getter garden-bed-box #:init-keyword #:garden-bed-box)
  (prev-garden #:accessor prev-garden #:init-value #f)
  (tiles #:accessor tiles #:init-value #())
  (current-plant #:accessor current-plant #:init-value "Cabbage")
  (read-only? #:getter read-only? #:init-keyword #:read-only? #:init-value #t))

(define-method (garden-bed (garden <garden-view>))
  (atomic-box-ref (garden-bed-box garden)))

(define-method (garden-name (garden <garden-view>))
  (atomic-box-ref (name-box garden)))

(define-method (on-boot (garden <garden-view>))
  (define title
    (make <label>
      #:name 'name
      #:text "Connecting..."
      #:font title-font
      #:position (vec2 32.0 (- %window-height 72.0))))
  (define tile-container
    (make <node-2d>
      #:name 'tile-container))
  (set! (width garden) %window-width)
  (set! (height garden) %window-height)
  (attach-to garden title tile-container)
  (center-horizontal-in-parent (& garden name))
  (refresh-garden garden)
  ;; Add buttons for selecting what to plant.
  (let loop ((plants '("Cabbage" "Sunflower"))
             (prev #f))
    (match plants
      (() #t)
      ((plant . rest)
       (let ((button (make <button>
                       #:on-click (lambda ()
                                    (set! (current-plant garden) plant))))
             (bg (make <canvas> #:name 'background))
             (sprite (make <sprite>
                       #:name 'sprite
                       #:texture (plant->texture plant)
                       #:position (vec2 8.0 0.0)))
             (label (make <label>
                      #:name 'label
                      #:text plant
                      #:font title-font))
             (pad 8.0))
         (set! (height button) 64.0)
         (set! (width button)
               (+ (* pad 3.0) (width sprite) (width label)))
         (set! (painter bg)
               (with-style ((fill-color db32-rope)
                            (stroke-color db32-oiled-cedar))
                 (fill-and-stroke
                  (rounded-rectangle (vec2 0.0 0.0)
                                     (width button)
                                     (height button)
                                     #:radius 2.0))))
         (attach-to button bg sprite label)
         (center-vertical-in-parent sprite)
         (center-vertical-in-parent label)
         (place-right sprite label #:padding pad)
         (if prev
             (begin
               (place-right prev button #:padding 32.0)
               (align-bottom prev button))
             (teleport button 32.0 32.0))
         (attach-to garden button)
         (loop rest button)))))
  (run-script garden
    (forever
     ;; Do not start asking for the garden bed information until the
     ;; name come across the network.  This is a really hacky way of
     ;; waiting until we've connected to the remote garden.
     (when (atomic-box-ref (name-box garden))
       (refresh-name garden)
       ((vat garden)
        (lambda ()
          (on (user garden)
              (lambda (user)
                (on (<- user 'inspect-garden)
                    (lambda (garden-bed)
                      (atomic-box-set! (garden-bed-box garden) garden-bed))))))))
     (sleep 0.5))))

(define-method (refresh-name (garden <garden-view>))
  (set! (text (& garden name)) (garden-name garden))
  (center-horizontal-in-parent (& garden name)))

(define (for-each-tile proc tiles)
  (vector-for-each
   (lambda (y row)
     (vector-for-each
      (lambda (x tile)
        (proc x y tile))
      row))
   tiles))

(define-method (tile-ref (garden <garden-view>) x y)
  (vector-ref (vector-ref (tiles garden) y) x))

(define-method (rebuild-tiles (garden <garden-view>))
  (let ((g (garden-bed garden))
        (container (& garden tile-container)))
    (match g
      ((bed-width bed-height bed-tiles)
       (for-each-tile
        (lambda (x y tile)
          (detach tile))
        (tiles garden))
       (set! (tiles garden)
             (vector-unfold
              (lambda (y)
                (vector-unfold
                 (lambda (x)
                   (let* ((painter (with-style ((fill-color db32-rope)
                                                (stroke-color db32-oiled-cedar))
                                     (fill-and-stroke
                                      (rectangle (vec2 0.0 0.0)
                                                 %tile-width
                                                 %tile-height))))
                          (bg (make <canvas>
                                #:name 'background
                                #:painter painter))
                          (sprite (make <sprite>
                                    #:name 'sprite
                                    #:texture null-texture))
                          (tile (make <garden-tile>
                                  #:tile-x x
                                  #:tile-y y)))
                     (set! (width tile) %tile-width)
                     (set! (height tile) %tile-height)
                     (attach-to tile bg sprite)
                     (attach-to container tile)
                     tile))
                 bed-width))
              bed-height))
       (for-each-tile
        (lambda (x y tile)
          (if (= y 0)
              (set! (position-y tile)
                    (* %tile-height (- bed-height 1)))
              (place-below (tile-ref garden x (- y 1)) tile))
          (unless (= x 0)
            (place-right (tile-ref garden (- x 1) y) tile)))
        (tiles garden))
       (set! (width container) (* bed-width %tile-width))
       (set! (height container) (* bed-height %tile-height))
       (center-in-parent container)))))

(define (plant->texture plant)
  (match plant
    ("Cabbage" cabbage-texture)
    ("Sunflower" sunflower-texture)
    (_ null-texture)))

(define-method (refresh-garden (garden <garden-view>))
  (let ((g (garden-bed garden))
        (prev-g (prev-garden garden)))
    (match g
      ((bed-width bed-height bed-tiles)
       (unless (match prev-g
                 ((_ _ prev-tiles)
                  (equal? prev-tiles bed-tiles))
                 (_ #f))
         (unless (match prev-g
                   ((prev-width prev-height _)
                    (and prev-width prev-height
                         (= bed-width prev-width)
                         (= bed-height prev-height)))
                   (_ #f))
           (rebuild-tiles garden))
         (for-each-tile
          (lambda (x y tile)
            (let ((plant (list-ref (list-ref bed-tiles y) x))
                  (sprite (& tile sprite)))
              (set! (texture sprite) (plant->texture plant))
              (center-in-parent sprite)))
          (tiles garden))
         (set! (prev-garden garden) g)))
      (_ #f))))

(define-method (update (garden <garden-view>) dt)
  (refresh-garden garden))

(define-method (plant-in-tile (garden <garden-view>) (tile <garden-tile>))
  (define (plant-ref plants name)
    (let loop ((plants plants))
      (match plants
        (() #f)
        (((k v) . rest)
         (if (string=? k name)
             v
             (loop rest))))))
  (when (current-plant garden)
    ((vat garden)
     (lambda ()
       (on (user garden)
           (lambda (user)
             (on (<- user 'get-approved-plants)
                 (lambda (plants)
                   (<- user 'plant
                       (tile-x tile)
                       (tile-y tile)
                       (plant-ref plants (current-plant garden)))))))))))

(define-method (dig-up-tile (garden <garden-view>) (tile <garden-tile>))
  ((vat garden)
   (lambda ()
     (on (user garden)
         (lambda (user)
           (<- user 'dig-up (tile-x tile) (tile-y tile)))))))

(define-method (garden-pick (garden <garden-view>) x y)
  (define (find-tile node)
    (cond
     ((not node)
      #f)
     ((or (is-a? node <garden-tile>)
          (is-a? node <button>))
      node)
     ((parent node)
      (find-tile (parent node)))
     (else #f)))
  (find-tile (pick garden (vec2 x y) (lambda _ #t))))

(define-class <garden-mode> (<major-mode>))

(define-method (do-plant-or-select (mode <garden-mode>) x y)
  (define garden (& (current-scene) garden))
  (define node (garden-pick garden x y))
  (cond
   ((is-a? node <button>)
    ((on-click node)))
   ((is-a? node <garden-tile>)
    (unless (read-only? garden)
      (plant-in-tile garden node)))))

(define-method (do-dig (mode <garden-mode>) x y)
  (define garden (& (current-scene) garden))
  (define tile (garden-pick garden x y))
  (when (and tile (not (read-only? garden)))
    (dig-up-tile garden tile)))

(bind-input <garden-mode> (mouse-press 'left) do-plant-or-select)
(bind-input <garden-mode> (mouse-press 'right) do-dig)

(define* (view-garden vat user #:key read-only?)
  (define garden-bed-box (make-atomic-box #f))
  (define garden-name-box (make-atomic-box #f))
  (vat
   (lambda ()
     (on (<- user 'get-garden-name)
         (lambda (name)
           (atomic-box-set! garden-name-box name)))))
  (run-catbird
   (lambda ()
     (let ((region (create-full-region #:name 'main))
           (scene (make <scene> #:name 'scratch)))
       (replace-scene region scene)
       (replace-major-mode scene (make <garden-mode>))
       (set! (camera region)
             (make <camera-2d>
               #:width %window-width
               #:height %window-height))
       (attach-to scene
                  (make <canvas>
                    #:name 'background
                    #:painter
                    (with-style ((fill-color db32-elf-green))
                      (fill
                       (rectangle (vec2 0.0 0.0)
                                  %window-width
                                  %window-height))))
                  (make <garden-view>
                    #:name 'garden
                    #:vat vat
                    #:user user
                    #:name-box garden-name-box
                    #:garden-bed-box garden-bed-box
                    #:read-only? read-only?))))
   #:title "Community Garden"
   #:width %window-width
   #:height %window-height))
