(define-module (community-garden host)
  #:use-module (community-garden actors)
  #:use-module (community-garden garden-bed)
  #:use-module (community-garden plant)
  #:use-module (community-garden view)
  #:use-module (goblins)
  #:use-module (goblins vrun)
  #:use-module (goblins ocapn ids)
  #:use-module (goblins ocapn captp)
  #:use-module (goblins ocapn netlayer onion)
  #:export (host-garden))

(define (host-garden garden-name user-name)
  (define garden-vat (spawn-vat))
  (define user-vat (spawn-vat))
  (define-vat-run garden-run garden-vat)
  (define-vat-run user-run user-vat)
  (define the-botanist (garden-run (spawn ^botanist)))
  (define the-garden-gate (garden-run (spawn ^garden-gate the-botanist)))
  (define sunflower/approved
    (garden-run ($ the-botanist 'approve-plant sunflower)))
  (define cabbage/approved
    (garden-run ($ the-botanist 'approve-plant cabbage)))
  (define our-garden
    (garden-run
     (spawn ^garden garden-name (make-garden-bed 8 8) the-garden-gate)))
  (define our-garden-community
    (garden-run
     (spawn ^garden-community our-garden)))
  (define user
    (user-run (<- our-garden-community 'register-gardener user-name)))
  (define onion-netlayer (garden-run (new-onion-netlayer)))
  (define mycapn (garden-run (spawn-mycapn onion-netlayer)))

  (garden-run
   (let ((community-sref ($ mycapn 'register our-garden-community 'onion)))
     (format #t "Connect to: ~a\n" (ocapn-id->string community-sref))))
  (view-garden user-vat user))
