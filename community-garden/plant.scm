(define-module (community-garden plant)
  #:use-module (srfi srfi-9)
  #:export (make-plant
            plant?
            plant-name
            plant-char

            sunflower
            cabbage
            winter-squash))

(define-record-type <plant>
  (make-plant name char)
  plant?
  (name plant-name)
  (char plant-char))

(define sunflower (make-plant "Sunflower" #\S))
(define cabbage (make-plant "Cabbage" #\C))
(define winter-squash (make-plant "Winter Squash" #\W))
