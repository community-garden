(define-module (community-garden join)
  #:use-module (community-garden actors)
  #:use-module (community-garden view)
  #:use-module (goblins)
  #:use-module (goblins vrun)
  #:use-module (goblins ocapn ids)
  #:use-module (goblins ocapn captp)
  #:use-module (goblins ocapn netlayer onion)
  #:use-module (ice-9 match)
  #:export (join-garden))

(define (join-garden name community-address)
  (define community-sref (string->ocapn-id community-address))
  (define vat (spawn-vat))
  (define-vat-run vat-run vat)
  (define onion-netlayer (vat-run (new-onion-netlayer)))
  (define mycapn (vat-run (spawn-mycapn onion-netlayer)))
  (define community-vow (vat-run ($ mycapn 'enliven community-sref)))
  (define gardener (vat-run (<- community-vow 'register-gardener name)))
  (view-garden vat gardener))
